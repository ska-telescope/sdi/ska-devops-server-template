"""
This is a template web server for ska-devops related jobs
"""

from fastapi import FastAPI, Header, HTTPException

from app.routers import members

app = FastAPI()


@app.get("/")
def test():
    return 200


async def get_token_header(x_token: str = Header(...)):
    """ Get token for security """
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


app.include_router(
    members.router,
    prefix="/members",
    tags=["members"],
    # dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)
