""" Example Route """
from typing import List

from fastapi import APIRouter, HTTPException

from app.models.member import Member

members: List[Member] = []
# mini version of FastAPI just for routing
router = APIRouter()


@router.get("/")
async def get_all_members():
    """ Return all members """
    return members


@router.post("/members/", response_model=Member)
async def create_new_member(member: Member):
    """ Create a new member """
    members.append(member)

    return member


@router.get("/members/{name}")
async def read_user(name: str):
    """ Get a member by its name """
    for member in members:
        if member.name == name:
            return {"member": member}

    raise HTTPException(status_code=404, detail="Member not found")
