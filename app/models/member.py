"""
Example class to illustrate basic fast-api capabilities
"""
from uuid import uuid4

from pydantic import validator

from app.models.person import Person


class Member(Person):
    """ Base Member class for pydantic validators """

    user_id: int = uuid4()
    prefer_language: str
    role: str

    @validator("prefer_language")
    def language_not_empty(prefer_language: str):
        """ Check if the language is empty or not """
        if prefer_language == "":
            raise ValueError("Cannot be empty")

        return prefer_language

    @validator("role")
    def role_not_empty(role: str):
        """ Check if the role is empty or not """
        if role == "":
            raise ValueError("Cannot be empty")

        return role
