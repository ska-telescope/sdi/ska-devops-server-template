"""
Example class to illustrate basic fast-api capabilities
"""
from pydantic import BaseModel, validator


class Person(BaseModel):
    """
    Example class to illustrate basic fast-api capabilities
    """

    name: str
    age: int

    @validator("name")
    def name_not_empty(cls, name):
        """ Check if the name is empty or not """
        if name == "":
            raise ValueError("Cannot be empty")

        return name

    @validator("age")
    def age_positive(cls, age):
        """ Check if the age is positive or not """
        if age < 0:
            raise ValueError("Age cannot be negative")

        return age
