Feature: Test Web Functionality

Scenario: Check Server 
    Given Main route
    Then  Return code 200

Scenario: Add Member with Json
    Given POST http request with body tests/files/member.json to /members/
    Then  Return code 200

Scenario: GET Member with Name
    Given GET http request to /members/John
    Then  Return code 200
